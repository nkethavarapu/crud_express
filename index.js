const { query } = require("express");
const express = require("express");
const mysql = require("mysql");
const app = express();
const PORT = 3000;

//creating connectiom
const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "india2001",

  database: "education",
});

con.connect(function (err) {
  if (err) throw err;
  console.log("connection established");
});
/**
 * this is a method to run sql queries
 * @param {string} query sql query
 */
const executeQuery = (query) => {
  con.query(query, function (err, result) {
    if (err) throw err;
    console.log("query executed");
    console.log(result);
  });
};
//create database
app.post("/1", function (req, res) {
  res.send(executeQuery(createDb("education")));
});
//read database
app.put("/2", function (req, res) {
  res.send(executeQuery(useDB("education")));
});
//create table
app.post("/3", function (req, res) {
  res.send(
    executeQuery(
      createTable("schools", [
        "name",
        "varchar(25)",
        "location",
        "varchar(25)",
        "id",
        "int(25)",
        "foundedYear",
        "int(25)",
      ])
    )
  );
});
//read tables

app.get("/4", function (req, res) {
  res.send(executeQuery(readTable("schools")));
});
//create records
app.post("/5", function (req, res) {
  res.send(executeQuery(
    //createRecord("schools", ["name", "st.Annes", "Location","Visakhapatnam","id","1909",  "foundedYear",  "1981"])
    //createRecord("schools", [ "name",  "timpany", "Location", "Visakhapatnam", "id", "1957", "foundedYear", "1964"])
    //createRecord("schools", [ "name",  "dps", "Location", "Visakhapatnam", "id", "977", "foundedYear", "1994"])
 
  ));
});
//update records
app.put("/6", function (req, res) {
  res.send(
    executeQuery(updateRecords("schools", "foundedYear", "1970", "id", "1909"))
  );
});
//read records
app.get("/7", function (req, res) {
  res.send(executeQuery(readRecords("schools")));
});
//Delete records
app.delete("/8", function (req, res) {
  res.send(executeQuery(deleteRecords("schools", "id", "977")));
});
//Delete table
app.delete("/9", function (req, res) {
  res.send(executeQuery(deleteTable("schools")));
});

//Delete database
app.delete("/10", function (req, res) {
  res.send(executeQuery(deleteDB("education")));
});

/**
 * this is a function to create
 * @param {string} db takes name of database
 *
 */
const createDb = (db) => {
  console.log("database created successfully");
  return `CREATE DATABASE ${db}`;
};
/**
 * this method is used to create table
 * @param {*} tbname name of the table
 * @param {*} tbSchema array that takes column name and its type respectively
 */
const createTable = (
  tbname,
  [col1, type1, col2, type2, col3, type3, col4, type4]
) => {
  console.log("Table created successfully");
  return ` CREATE TABLE ${tbname} (${col1} ${type1},${col2} ${type2},${col3} ${type3},${col4} ${type4})`;
};

/**
 * method to specify the database to be used
 * @param {string} dbName name of existing database
 *
 */

const useDB = (dbName) => {
  console.log("Read database");
  return `use ${dbName}`;
};
/**
   * this method inserts values into table
   * @param {*} tbname name of the table
   * @param {*} param1 array of colnames and values
  
   */
const createRecord = (
  tbname,
  [col1, value1, col2, value2, col3, value3, col4, value4]
) => {
  console.log("record created successfully");
  return `INSERT INTO ${tbname} (${col1}, ${col2}, ${col3}, ${col4}) VALUES ('${value1}',' ${value2}', '${value3}', '${value4}')`;
};
/**
 * this function reads the records of given table
 * @param {*} tableName name of the table
 *
 */

const readRecords = (tbname) => {
  console.log("record read successfully");
  return `SELECT * FROM ${tbname}`;
};
/**
 * deletes the table
 * @param {string} tableName name of the table
 * @returns
 */
const deleteTable = (tbname) => {
  console.log("table deleted successfully");
  return `DROP TABLE ${tbname}`;
};
/**
 * this is a function to update records in table
 * @param {*} tbname name othe table
 * @param {*} updateCol column to be updated
 * @param {*} updateValue updated column value
 * @param {*} keyCol
 * @param {*} keyValue
 * @returns
 */
const updateRecords = (tbname, updateCol, updateValue, keyCol, keyValue) => {
  return `UPDATE ${tbname} SET ${updateCol} = '${updateValue}' WHERE ${keyCol} = ${keyValue}`;
};

/**
 * this is a function to delete database
 * @param {*} dbName name of database
 */

const deleteDB = (dbName) => {
  console.log("database deleted successfully");
  return `DROP DATABASE ${dbName}`;
};
//this is a function to delete records
const deleteRecords = (tbname, keyCol, keyVal) => {
  console.log("record deleted successfully");
  return `DELETE FROM ${tbname} WHERE ${keyCol}='${keyVal}'`;
};

const readTable = (tbname) => {
  return `DESC ${tbname}`;
};

//listening to port
app.listen(PORT, function (err) {
  if (err) {
    console.log("error in setup");
  } else {
    console.log("server satrted");
  }
});
